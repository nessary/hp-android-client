package net.hserver.hp_client.config;

import net.hserver.hp_client.domian.vo.UserVo;

public class ConstConfig {

    public final static String IP = "ksweb.club";
//    public final static String IP = "192.168.123.220";

    public final static Integer PORT = 7731;

    public final static String URL = "http://" + IP + ":9090";

    public final static String USERNAME = "USERNAME";

    public final static String PASSWORD = "PASSWORD";

    public static UserVo USER_VO = null;

    public static final String USER_IP="USER_IP";

    public static final String USER_PORT="USER_PORT";

    public static final String ABOUT="ABOUT";

}
