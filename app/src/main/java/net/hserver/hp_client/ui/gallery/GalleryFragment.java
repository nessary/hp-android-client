package net.hserver.hp_client.ui.gallery;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

import net.hserver.hp_client.MainActivity;
import net.hserver.hp_client.R;
import net.hserver.hp_client.config.ConstConfig;
import net.hserver.hp_client.domian.vo.UserVo;
import net.hserver.hp_client.service.UserService;
import net.hserver.hp_client.ui.home.HomeFragment;

import java.util.ArrayList;
import java.util.List;

public class GalleryFragment extends Fragment {
    private UserService userService = new UserService();
    private List<String> list = new ArrayList<>();
    private ArrayAdapter<String> stringArrayAdapter;
    private LoadMoreListView listView;
    private Integer page = 1;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_gallery, container, false);
        listView = root.findViewById(R.id.listView);
        stringArrayAdapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_list_item_1, list);
        listView.setAdapter(stringArrayAdapter);
        listView.setOnLoadMoreListener(new LoadMoreListView.OnLoadMoreListener() {
            @Override
            public void onloadMore() {
                if (ConstConfig.USER_VO != null) {
                    userService.getLog(handler, page, ConstConfig.USER_VO.getUsername());
                }
            }
        });
        userService.getLog(handler, page, ConstConfig.USER_VO.getUsername());
        return root;
    }


    @SuppressLint("HandlerLeak")
    public final Handler handler = new Handler() {
        @SuppressLint("SetTextI18n")
        @Override
        public void handleMessage(@NonNull Message msg) {
            if (msg.what == 1) {
                try {
                    Object obj = msg.obj;
                    JSONObject jsonObject = JSON.parseObject(obj.toString());
                    JSONArray listArray = jsonObject.getJSONArray("list");
                    if (listArray.size() != 10) {
                        page = -1;
                    } else {
                        page++;
                    }
                    for (int i = 0; i < listArray.size(); i++) {
                        JSONObject jsonObject1 = listArray.getJSONObject(i);
                        String port = jsonObject1.getString("port");
                        String connectNum = jsonObject1.getString("connectNum");
                        String packNum = jsonObject1.getString("packNum");
                        String createTime = jsonObject1.getString("createTime");
                        String receive = jsonObject1.getString("receive");
                        String send = jsonObject1.getString("send");
                        StringBuilder stringBuilder = new StringBuilder();
                        stringBuilder.append("端口：").append(port).append("\n");
                        stringBuilder.append("连接数累计：").append(connectNum).append("\n");
                        stringBuilder.append("数据包累计：").append(packNum).append("\n");
                        stringBuilder.append("发送字节大小：").append(receive).append("\n");
                        stringBuilder.append("接收字节大小：").append(send).append("\n");
                        stringBuilder.append("创建时间：").append(createTime).append("\n");
                        list.add(stringBuilder.toString());
                    }
                    stringArrayAdapter.notifyDataSetChanged();
                    listView.setLoadCompleted();
                } catch (Throwable e) {
                    e.printStackTrace();
                }
            }else {
                Toast.makeText(getContext(),msg.obj.toString(),Toast.LENGTH_SHORT).show();
                listView.setLoadCompleted();
            }
            super.handleMessage(msg);
        }
    };

}