package net.hserver.hp_client.ui.gallery;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ListView;
import android.widget.Toast;

import net.hserver.hp_client.R;

/**
 */
public class LoadMoreListView extends ListView implements AbsListView.OnScrollListener  {

    private Context mContext;
    private View mFootView;
    private int mTotalItemCount;
    private OnLoadMoreListener mLoadMoreListener;
    private boolean mIsLoading=false;

    public LoadMoreListView(Context context) {
        super(context);
        init(context);
    }

    public LoadMoreListView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    public LoadMoreListView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    private void init(Context context){
        this.mContext=context;
        mFootView= LayoutInflater.from(context).inflate(R.layout.header_view_layout,null);
        setOnScrollListener(this);
    }


    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {
        int lastVisibleIndex = view.getLastVisiblePosition();
        if (!mIsLoading
                && scrollState == OnScrollListener.SCROLL_STATE_IDLE
                && lastVisibleIndex == mTotalItemCount-1
                ) {
            mIsLoading=true;
            addFooterView(mFootView);
            if (mLoadMoreListener!=null) {
                mLoadMoreListener.onloadMore();
            }
        }

    }


    @Override
    public void onScroll(AbsListView absListView, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
        mTotalItemCount=totalItemCount;
        if (firstVisibleItem == 0) {
//            Toast.makeText(mContext, "滑到顶部", Toast.LENGTH_SHORT).show();
        }
        if (visibleItemCount + firstVisibleItem == totalItemCount) {
//            Toast.makeText(mContext, "滑到底部",Toast.LENGTH_SHORT).show();
        }

    }

    public void setOnLoadMoreListener(OnLoadMoreListener listener){
        mLoadMoreListener=listener;
    }

    public interface OnLoadMoreListener{
        void onloadMore();
    }

    public void setLoadCompleted(){
        mIsLoading=false;
       removeFooterView(mFootView);
//        removeHeaderView(mFootView);
    }

}